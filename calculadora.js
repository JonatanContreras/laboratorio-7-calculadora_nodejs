//Importar la clase express

import express from 'express';

//Crear un objeto express
const app=express();
let puerto=3000;

//Crear una ruta
app.get("/bienvenida", (req,res)=>{

    res.send("Bienvenido al mundo de backend")
});

//Suma
app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number( peticion.query.a)+Number(peticion.query.b);

    respuesta.send("El resultado de la suma es: "+resultado.toString());
});

//Resta
app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number( peticion.query.a)-Number(peticion.query.b);

    respuesta.send("El resultado de la resta es: "+resultado.toString());
});

//Multiplicar
app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number( peticion.query.a) * Number(peticion.query.b);

    respuesta.send("El resultado de la multiplicación es: "+resultado.toString());
});

//Dividir
app.get("/dividir", (peticion, respuesta) => {
    let resultado;
    let a = Number(peticion.query.a);
    let b = Number(peticion.query.b);
    if (b == 0) {
        resultado = "No se puede realizar la operacion";
    } else {
        resultado = a / b;
    }
    respuesta.send("El resultado de la multiplicación es: "+resultado.toString());
});

//Modular
app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number( peticion.query.a) % Number(peticion.query.b);

    respuesta.send("El resultado de la modulación es: "+resultado.toString());
});

//Iniciar un servidor node

app.listen(puerto, function(){
    console.log("Servidor escuchando en el puerto" +puerto);
})